#!/usr/bin/env fish

sudo pacman -S --noconfirm --needed git curl fish docker docker-compose python-pip vim base-devel
